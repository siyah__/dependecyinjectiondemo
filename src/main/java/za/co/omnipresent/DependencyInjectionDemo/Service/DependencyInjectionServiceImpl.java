package za.co.omnipresent.DependencyInjectionDemo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.omnipresent.DependencyInjectionDemo.Models.Human;
import za.co.omnipresent.DependencyInjectionDemo.Models.Vehicle;

public class DependencyInjectionServiceImpl implements DependencyInjectionService {

    @Autowired
    private Human human;

    @Override
    public String displayRoutine(){

        human.setName("Siya");

        return human.eat() + " ---- " +human.takeBath();
    }
}
