package za.co.omnipresent.DependencyInjectionDemo.Models;

import lombok.Data;

@Data
public class Vehicle {

    public Vehicle() {
        System.out.println("vehicle object created.");
    }

    public void drive(){
        System.out.println("vrooom vrooom");
    }
}
