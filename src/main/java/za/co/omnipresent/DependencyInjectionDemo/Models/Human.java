package za.co.omnipresent.DependencyInjectionDemo.Models;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Human {

    private String name;

    public Human() {
        System.out.println("object created");
    }

    public void performDailyRoutine(){
        eat();
        sleep();
        takeBath();
    }
    public String eat(){
        System.out.println(name+ " is eating now");
        return name+" is eating now";
    }

    public String sleep(){
        System.out.println(name+ " is sleeping now");
        return name+" is sleeping now";
    }

    public String takeBath()
    {
        System.out.println(name+ " is now bathing.");
        return name+" is now bathing.";
    }


}
