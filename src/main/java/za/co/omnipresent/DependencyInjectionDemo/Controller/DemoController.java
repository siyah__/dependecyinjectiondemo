package za.co.omnipresent.DependencyInjectionDemo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.omnipresent.DependencyInjectionDemo.Service.DependencyInjectionService;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/test")
    public String dependencyInjection(){
        return dependencyInjectionService.displayRoutine();
    }

    @Autowired
    private DependencyInjectionService dependencyInjectionService;
}
