package za.co.omnipresent.DependencyInjectionDemo.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import za.co.omnipresent.DependencyInjectionDemo.Models.Human;
import za.co.omnipresent.DependencyInjectionDemo.Service.DependencyInjectionService;
import za.co.omnipresent.DependencyInjectionDemo.Service.DependencyInjectionServiceImpl;

@Configuration
public class BeanConfig {

    @Bean
    public DependencyInjectionService dependencyInjectionService(){
        return new DependencyInjectionServiceImpl();
    }

    @Bean
    public Human human(){
        return new Human();
    }
}
